# OpenID Connect

This repo shows an example of how to connect to the Octopus API using OpenID Connect. It exchanges a GitLab ID token for an Octopus access token and then makes an example request to the Octopus API to get details of the service account that has been authenticated.

See https://octopus.com/docs/octopus-rest-api/openid-connect/other-issuers for more information on using OpenID Connect in Octopus and https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html for more information on how to use ID tokens in GitLab.
